/*var http = require('http');

http.createServer((request,response) =>{
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.end('Hola a todos y a todas!');
}).listen(8080);

console.log('Servidor ejecutandose en el puerto 8080...');*/

/*
'use strict'

const port = process.env.PORT || 3000;

const express = require('express');
const logger = require('morgan');

const app = express();

app.use(logger('dev'));

app.get('/hola/:name', (req, res) => {
    res.status(200).send({mensaje: `Hola ${req.params.name} desde JSON!`});
});

app.listen(8080, () => {
    console.log(`API REST ejecutandose en http://localhost:${port}/hola/:unNombre`);
});

//Api rest tipo crud

'use strict'
const port = process.env.PORT || 3000;
const express = require('express');
const logger = require('morgan');
const app = express();
// Declaramos los middleware
app.use(logger('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
// probar con: tiny, short, dev, common, combined
// Implementamos el API RESTFul a través de los métodos
app.get('/api/products', (req, res) => {
res.status(200).send({products: []});
});
app.get('/api/products/:productID', (req, res) => {
res.status(200).send({products: `${req.params.productID}`});
});
app.post('/api/products', (req, res) => {
console.log(req.body);
res.status(200).send({products: 'El producto se ha recibido'});
});
app.put('/api/products/:productID', (req, res) => {
res.status(200).send({products: `${req.params.productID}`});
});
app.delete('/api/products/:productID', (req, res) => {
res.status(200).send({products: `${req.params.productID}`});
});
// Lanzamos nuestro servicio API
app.listen(port, () => {
    console.log(`API REST ejecutándose en http://localhost:${port}/api/product`);
    });
*/

/* //CON BASE DE DATOS
'use strict'
const port = process.env.PORT || 3000
const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const app = express();
var db = mongojs("SD");
var id = mongojs.ObjectID;

// Enlazamos con la DB "SD"
// Función para convertir un id textual en un objectID
// middlewares
app.use(logger('dev'));
// probar con: tiny, short, dev, common, combined
app.use(express.urlencoded({extended: false})) // parse application/x-www-form-urlencoded
app.use(express.json())
// parse application/json
// añadimos un trigger previo a las rutas para dar soporte a múltiples colecciones
app.param("coleccion", (req, res, next, coleccion) => {
console.log('param /api/:coleccion');
console.log('colección: ', coleccion);

req.collection = db.collection(coleccion);
return next();
});
// routes
app.get('/api', (req, res, next) => {
console.log('GET /api');
console.log(req.params);
console.log(req.collection);
db.getCollectionNames((err, colecciones) => {
if (err) return next(err);
res.json(colecciones);
});
});
app.get('/api/:coleccion', (req, res, next) => {
    req.collection.find((err, coleccion) => {
    if (err) return next(err);
    res.json(coleccion);
    });
});
app.get('/api/:coleccion/:id', (req, res, next) => {
    req.collection.findOne({_id: id(req.params.id)}, (err, elemento) => {
    if (err) return next(err);
    res.json(elemento);
    });
});
app.post('/api/:coleccion', (req, res, next) => {
    const elemento = req.body;

if (!elemento.nombre) {
    res.status(400).json ({
    error: 'Bad data',
    description: 'Se precisa al menos un campo <nombre>'
    });

} else {
    req.collection.save(elemento, (err, coleccionGuardada) => {
    if(err) return next(err);
    res.json(coleccionGuardada);
    });
}
});
app.put('/api/:coleccion/:id', (req, res, next) => {
    let elementoId = req.params.id;
    let elementoNuevo = req.body;

    req.collection.update({_id: id(elementoId)},
    {$set: elementoNuevo}, {safe: true, multi: false}, (err, elementoModif) => {
    if (err) return next(err);
    res.json(elementoModif);
    });
});
app.delete('/api/:coleccion/:id', (req, res, next) => {
    let elementoId = req.params.id;

    req.collection.remove({_id: id(elementoId)}, (err, resultado) => {
        if (err) return next(err);
        res.json(resultado);
    });
});
// Iniciamos la aplicación
app.listen(port, () => {
    console.log(`API REST ejecutándose en http://localhost:${port}/api/:coleccion/:id`);
});
*/

'use strict'
const port = process.env.PORT || 3000
const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const app = express();
var db = mongojs("SD"); // Enlazamos con la DB "SD"
var id = mongojs.ObjectID; // Función para convertir un id textual en un objectID
// middlewares
var allowMethods = (req, res, next) => {
res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
return next();
};
var allowCrossTokenHeader = (req, res, next) => {
res.header("Access-Control-Allow-Headers", "token");
return next();
};
var auth = (req, res, next) => {
if(req.headers.token === "password1234") {
return next();
} else {
return next(new Error("No autorizado"));
};
};
app.use(logger('dev'));
// probar con: tiny, short, dev, common, combined
app.use(express.urlencoded({extended: false})) // parse application/x-www-form-urlencoded
app.use(express.json())
// parse application/json
app.use(allowMethods);
app.use(allowCrossTokenHeader);
// routes
app.param("coleccion", (req, res, next, coleccion) => {
req.collection = db.collection(coleccion);
return next();
});
app.get('/api', (req, res, next) => {
    db.getCollectionNames((err, colecciones) => {
    if (err) return next(err);
    res.json(colecciones);
    });
    });
    app.get('/api/:coleccion', (req, res, next) => {
    req.collection.find((err, coleccion) => {
    if (err) return next(err);
    res.json(coleccion);
    });
    });
    app.get('/api/:coleccion/:id', (req, res, next) => {
    req.collection.findOne({_id: id(req.params.id)}, (err, elemento) => {
    if (err) return next(err);
    res.json(elemento);
    });
    });
    app.post('/api/:coleccion', auth, (req, res, next) => {
    const elemento = req.body;
    if (!elemento.nombre) {
        res.status(400).json ({
            error: 'Bad data',
            description: 'Se precisa al menos un campo <nombre>'
            });
            } else {
            req.collection.save(elemento, (err, coleccionGuardada) => {
            if(err) return next(err);
            res.json(coleccionGuardada);
            });
            }
        });
            app.put('/api/:coleccion/:id', auth, (req, res, next) => {
            let elementoId = req.params.id;
            req.collection.update({_id: id(elementoId)}, {$set: req.body},
            {safe: true, multi: false}, (err, result) => {
            if (err) return next(err);
            res.json(result);
            });
            });
            app.delete('/api/:coleccion/:id', auth, (req, res, next) => {
            let elementoId = req.params.id;
            req.collection.remove({_id: id(elementoId)}, (err, resultado) => {
            if (err) return next(err);
            res.json(resultado);
            });
            });
            // Iniciamos la aplicación
            app.listen(port, () => {
            console.log(`API REST ejecutándose en http://localhost:${port}/api/:coleccion/:id`);
            });

